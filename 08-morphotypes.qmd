---
title: "Comparing morphotypes"
authors:
    - name: Floriane Remy
      orcid: 0000-0002-2258-7207
      affiliations:
          - CNRS, Univ. Bordeaux, MCC – UMR 5199 PACEA
    - name: Frédéric Santos
      orcid: 0000-0003-1445-3871
      affiliations:
          - CNRS, Univ. Bordeaux, MCC – UMR 5199 PACEA
lang: en
bibliography: "biblio.bib"
---

```{r}
#| echo: false
#| eval: true
#| message: false
#| results: hide
#| warning: false
source("./scripts/script.R")
```

Once the inter-individual variability has been described, and the factors explaining it highlighted, it can be useful to represent this morphological variation. To do so, the mean shape (also called *morphotype* or *consensus*) of each group of interest may be plotted together, so they could be compared.

Depending on the nature of the available data (2D or 3D), these morphological differences can either be represented as vectors, through a thin-plate spline or a surface color map.

## Visualizing the differences between two configurations
The R function `geomorph::plotRefToTarget()` can be used to visualize the shape differences between two different landmarks configurations (as already illustrated in [another section](04-PCA-outliers.qmd)). For instance, @fig-diffmean1 shows the differences between the global mean shape of the sample, and one given individual.

```{r}
#| fig-cap: Shape differences between the first individual and the global mean shape.
#| label: fig-diffmean1
## Global mean shape:
Mglob <- mshape(gpa$coords)

## Shape differences with the first individual:
plotRefToTarget(Mglob, gpa$coords[, , 1], method = "TPS")
mtext("TPS")
```

## Comparing the morphotypes of various groups
The previous function can thus be used to compare the mean shapes of several groups, for instance the mean shapes of females and males.

The following code allows for the computation of female and male morphotypes respectively.

```{r}
## Compute female and males morphotypes respectively.
Mfemales <- mshape(gpa$coords[ , , meta$Sex == "F"])
Mmales <- mshape(gpa$coords[ , , meta$Sex == "M"])
```


::: {.callout-note icon=false}

## Exercise

1. Use the function `geomorph::plotRefToTarget()` to compare the mean shapes of females and males. What can you say about the amount of sexual dimorphism?
2. Which limits can you see for this approach? How to fix them?

:::

::: {.callout-tip icon=false collapse=true}

## Solution

Hint for the R code:

```{r}
#| fig-cap: Shape differences between female and male morphotypes.
#| label: fig-diffmales
plotRefToTarget(Mfemales, Mmales, method = "TPS")
mtext("TPS")
```

:::
