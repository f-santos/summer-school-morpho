---
title: "Loading data"
authors:
    - name: Floriane Remy
      orcid: 0000-0002-2258-7207
      affiliations:
          - CNRS, Univ. Bordeaux, MCC – UMR 5199 PACEA
    - name: Frédéric Santos
      orcid: 0000-0003-1445-3871
      affiliations:
          - CNRS, Univ. Bordeaux, MCC – UMR 5199 PACEA
lang: en
bibliography: "biblio.bib"
---

## Preamble: create your Rstudio project
A [Git repository](https://gitub.u-bordeaux.fr/fsantos/data-morpho) was created to host some data files, in various formats. As a first step, you will have to make a local copy of this repository on your computer, and turn it into an Rstudio *project*.

1. Download the whole content of the repo [as a zip file](https://gitub.u-bordeaux.fr/fsantos/data-morpho/-/archive/master/data-morpho-master.zip), and unzip it in a given folder on your computer.
2. Open Rstudio, and open the menu File > New Project.
3. Choose the type "Existing directory", and create a project in the folder created in step 1.

## Usual data formats in morphometrics
Usually, the data that we handle in morphometrics are not natively in a classic "*tidy*" format (one row per individual, one column per variable). They can also sometimes be in specific file formats (`.dta`, `.xml`, ...).

### Landmarks coordinates in text files
In the best and simplest case, the software you used to place 2D or 3D landmarks on your specimens will produce a plain text file (`.csv` or `.txt`) for each individual, with one row per landmark and one column per coordinate (`x`, `y` and optionally `z`) each. The basic R function `read.table()` will then be convenient to load your data, as in the example below.

```{r}
## Exemple d'importation d'un fichier txt :
colb <- read.table(
  file = "./scallops/colb01.txt",
  header = FALSE,   # pas d'intitulés de colonnes
  sep = ",",        # séparateur de colonnes
  dec = "."         # séparateur décimal
)
head(colb)          # visualiser un extrait du fichier
```

### Specific data formats
However, some data acquisition software will return non-standard file formats. A (non-exhaustive) list of examples is given below; the corresponding files can be explored in the `formats` folder of the [Git repository](https://gitub.u-bordeaux.fr/fsantos/data-morpho).

#### DTA files
They are created (for instance) by the software [Landmark](http://graphics.idav.ucdavis.edu/research/EvoMorph). The data for all your individuals is generally stored in one single file. The function `read.lmdta()` from the R package `{Morpho}` can load this kind of files:

```{r}
## Load a dta file:
dta <- Morpho::read.lmdta(file = "./formats/example_landmark.dta")
## Structure of the R object created:
str(dta)
```
```{r}
## Display the coordinates for the first individual:
print(dta$arr[, , 1])
```

#### PLY files
Polygon files are (for instance) created by Avizo. They may represent either landmarks data, or surfaces. They can be loaded using the function `read.ply()` from the package `{geomorph}`, or using the function `vcgImport()` from the package `{Rvcg}`.

```{r eval=FALSE, include=TRUE}
## Load a surface in PLY format and visualize it:
ply <- Rvcg::vcgImport(file = "./formats/example_avizo.ply", clean = TRUE)
rgl::shade3d(ply, col = "grey")
```

#### TPS files
They are created by the software [tpsDIG2](http://www.diatom.org/software/efa/imageJ&tpsDIG.htm). Once again, the data for several individuals are generally stored in one single file. The function `readland.tps()` from the pakage `{geomorph}` (or the function `readallTPS()` from the package `{Morpho}`) allows you to load such files:

```{r}
#| message: false
#| warning: false
## Load a TPS file:
tps <- geomorph::readland.tps(file = "./formats/example_tpsdig.TPS",
                              specID = "imageID")
## Display the coordinates of the first shape:
print(tps[, , 1])
```

#### XML files
They are created for instance by the software [Viewbox](http://www.dhal.com/viewbox.htm). The function `read_viewbox()` from the package `{anthrostat}` allows you to load such files:
```{r}
#| warning: false
## Load an XML file:
xml <- anthrostat::read_viewbox("./formats/example_viewbox.xml")
head(xml)
```

There are many other classical file formats in morphometrics (e.g., JSON, NTS, ...). You can find specific function to load them in the R packages `{geomorph}` and `{Morpho}`.

## Load several files at once
For DTA or TPS formats for instance, we generally have several individuals in one single file, so that the corresponding R functions import all the individuals in one single instruction. In other cases, however, we have one file (CSV, TXT, etc.) per individual, so that we have to load a large set of data files. Obviously, if we have 80 files, we must *not* write 80 `read.table()` instructions manually!

Let's inspect the sub-folder `scallops`:
```{r}
files <- list.files("./scallops")
print(files)
```
There are 5 TXT files in this folder. How to load them with one single R instruction?

For CSV or TXT files, the function `Morpho::read.csv.folder()`^[I.e., the function `read.csv.folder()` from the package `{Morpho}`. In R, we can write a function with the general syntax `package::function()`.] loads all the files from a given directory at once, and returns an *array*. Of course, all files must have the same number of rows and columns, and have the same extension.

```{r echo=TRUE, results="hide"}
#| results: hide
## Load all TXT files:
lmarray2 <- Morpho::read.csv.folder(
  folder = "./scallops/", # the folder to load
  x = 1:44,               # the rows to read
  y = 1:3,                # the columns to read
  header = FALSE,         # no column names in the files
  dec = ".",              # decimal point
  sep = ",",              # field separator
  pattern = "*.txt"       # pattern of the files to load
)
## Print the array:
print(lmarray2$arr)
```

## In practice
In this course, we will use the data contained in the file [`rats.TPS`](https://gitub.u-bordeaux.fr/fsantos/data-morpho/-/blob/master/data/rats.TPS?ref_type=heads). In this file are stored the coordinates of 15 fixed landmarks (these are the first 15 rows of each individual) and 88 semilandmarks taken on rats mandibles, as shown on @fig-rats.

![Protocol for landmarks and semilandmarks.](./images/semilandmarks_rats.jpg){#fig-rats width=80%}

::: {.callout-note icon=false}

## Exercise

1. (Optional) Open the file `rats.TPS` using a text editor, inspect it and try to understand its structure.
2. Load this TPS file in R.
3. How many individuals do we have in this file?

:::

::: {.callout-tip icon=false collapse=true}

## Solution

In this TPS file, the rows `LM=103` suggest that we have 103 landmarks per individual. Each landmark has two coordinates $(x,y)$, so that we have an array of 103 rows and 2 columns per individual. The file `ID` indicates the identifier of each individual.

To load the TPS file in R:


```{r}
## Load TPS file:
rats <- geomorph::readland.tps(
  file = "./data/rats.TPS",
  specID = "imageID"
)
```

As we can see, `rats` is an *array*:
```{r}
class(rats)
```

We can display the dimension of this array:
```{r}
dim(rats)
```

This means that we have 159 individuals in total, each of them with 103 bi-dimensional landmarks.

Finally, we can display the coordinates of the first five landmarks for the first individual:
```{r}
print(rats[1:5, , 1])
```

:::

## Loading CSV files
Finally, we have some metadata (sex, age, weight, ...) about the individuals. These data are stored in a CSV file, which is the recommended format for loading "usual" flat data in R. This data can be loaded using the built-in `read.csv2()` function:

```{r}
## Load CSV file:
meta <- read.csv2(
  file = "./data/rats.csv",
  row.names = 1,
  stringsAsFactors = TRUE,
  na.strings = ""
)
## Summarise the dataframe:
summary(meta)
```

One useful thing, for subsequent statistical analyses, may be to convert the still unordered factor `Age_class` into an ordered factor. By default, the levels (i.e., age classes) have no specific order and appear in alphabetical order in all plots, which is clearly not convenient here.

```{r}
## Convert age casses into an ordered factor:
meta$Age_class <- factor(
  meta$Age_class,
  ordered = TRUE,
  levels = c("Juvenile", "Sub-adult", "Adult", "Older adult")
)
```

As a (very strongly recommended!) good practice, we should make sure that this dataframe includes the same number of individuals as the TPS file, and that they are given in the same order.

```{r}
## Check dimension of CSV file:
dim(meta)
```

```{r}
## Check consistency of ordering between TPS and CSV:
all(dimnames(rats)[[3]] == rownames(rats))
```
