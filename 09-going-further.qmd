---
title: "Going further"
authors:
    - name: Floriane Remy
      orcid: 0000-0002-2258-7207
      affiliations:
          - CNRS, Univ. Bordeaux, MCC – UMR 5199 PACEA
    - name: Frédéric Santos
      orcid: 0000-0003-1445-3871
      affiliations:
          - CNRS, Univ. Bordeaux, MCC – UMR 5199 PACEA
lang: en
bibliography: "biblio.bib"
---

Many other topics have not been covered in this course, but here are some useful pointers.

1. In particular when studying sexual dimorphism, you might want to build decisional models to know whether it is possible to predict a qualitative variable (e.g., the sex) from the procrustes coordinates. A wide family of *supervised classification* methods is available to that end, but the most common is the discriminant analysis [@rencher2012_DiscriminantAnalysisDescription], implemented for instance in the R function `Morpho::CVA()`^[In morphometrics, people often refer to discriminant analysis as "canonical variates analysis". This is not the standard terminology in mathematics, and is also a bit confusing since this is the name of another statistical method.]. Its help page provides useful examples.
2. A closely related method, often used to estimate the geographical origin of an individual, is the use of "typicality probabilities" [@wilson1981_ComparingFossilSpecimens;@mizoguchi2011_TypicalityProbabilitiesLate], implemented in `Morpho::typprobClass()`.
3. We did not even mention *landmarks-free methods* in morphometrics, which were covered in other courses of the summer school.

## References {-}