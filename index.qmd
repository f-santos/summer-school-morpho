---
title: "Morphometrics with R: Practical session"
subtitle: "University of Bordeaux, Summer school 2024"
authors:
    - name: Floriane Rémy
      orcid: 0000-0002-2258-7207
      affiliations:
          - CNRS, Univ. Bordeaux, MCC – UMR 5199 PACEA
    - name: Frédéric Santos
      orcid: 0000-0003-1445-3871
      affiliations:
          - CNRS, Univ. Bordeaux, MCC – UMR 5199 PACEA
date: '07/05/2024'
date-format: "DD MMM YYYY"
lang: en
citeproc: true
bibliography: biblio.bib
csl: apa.csl
---

## Course overview
This website is a companion for a pratical session "Morphometrics with R" which is part of the 
[Summer school "Digital Archaeology"](https://sciences-archeologiques.u-bordeaux.fr/gpr/productions/ecole-detesummer-school) (University of Bordeaux). This short practical session requires no previous knowledge of R, so that it will cover the most elementary notions both for R as a language, and for applied morphometrics.

## Why use R?
There are other (very good) software out there for geometric morphometrics and statistics, such as [PAST](https://www.nhm.uio.no/english/research/infrastructure/past/) [@hammer2006_PaleontologicalDataAnalysis] or [MorphoJ](https://morphometrics.uk/MorphoJ_page.html). But R has several key advantages:

- quantity and quality of documentation;
- access to a wider range of advanced methods;
- ease of automating repetitive tasks;
- large community of users;
- ability to work following [literate programming](https://en.wikipedia.org/wiki/Literate_programming) principles [@knuth1984_LiterateProgramming] ;
- reproducible and transparent analyses (when done right!).

## External resources

- [A general introduction to R](https://intro2r.com/), by Alex Douglas et al.
- @claude2008_Morphometrics is a very complete reference, although a bit old, so that it does not document the most recent and efficient R packages for doing morphometrics.
- @zelditch2012_GeometricMorphometricsBiologists is another good reference, with many concrete use cases and R scripts provided in appendices.
- @elewa2010_MorphometricsNonmorphometricians and 
@webster2010_PracticalIntroductionLandmarkBased are other excellent resources to begin with morphometrics -- although not specifically in R.

## My R installation does not work!
You can still follow the course with a server session by following this link:  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/f-santos%2Fbinder_ssda_2024/HEAD)


## References {-}