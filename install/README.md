Required software
=================

Most of the practical sessions of the Summer School will use free software. Therefore, at least some of the practical applications can be done on your personal computer if you wish.

If you want to work on your own computer, make sure that you have installed the following software.

## Office software
To handle, visualize or edit the data files, you will need:

- a spreadsheet (such as [LibreOffice Calc](https://www.libreoffice.org/download/download/), that we strongly recommend)
- a good text editor (e.g., [Notepad++](https://notepad-plus-plus.org/) for Windows, or [SublimeText](http://www.sublimetext.com/download) for other platforms).

## MorphoJ
[MorphoJ](https://morphometrics.uk/MorphoJ_page.html) is a free software for geometric morphometrics. It is available on all operating systems.

## R
Please make sure that you have a *recent* version of R installed on your computer (>= 4.4.0). It can be downloaded from the [official R website](https://cran.r-project.org/).

If you currently have a much older version installed, please uninstall it and download the latest version instead.

## Complementary build environment (for Mac or Windows users only)
- Mac OS: install [Xquartz](https://www.xquartz.org/).
- Windows: install [Rtools](https://cran.r-project.org/bin/windows/Rtools/rtools44/rtools.html) in its latest version (Rtools 4.4). 

Note that Linux users have nothing to install for this step.

## Rstudio
[Rstudio](https://posit.co/download/rstudio-desktop/#download) is an integrated development environment available for all operating systems.

## R packages
Run the following commands into the R console to install some useful packages:

```r
install.packages(c("ade4", "car", "corrplot", "devtools", "epiR",
                   "factoextra", "FactoMineR", "geomorph", "ggpubr",
                   "irr", "lattice", "MASS", "Morpho", "rgl", "Rvcg",
                   "scatterplot3d", "shapes", "tidyverse", "vegan"),
                 dep = TRUE, repos = "https://cran.wu.ac.at/")
```

## Help!
For any assistance, please contact us (Frédéric Santos or Floriane Rémy) directly by e-mail.
